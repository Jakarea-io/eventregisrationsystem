<?php

include "config.php";
include "common.php";
include "class.phpmailer.php";

$dated = date('Y-m-d H:i:s');
$task = filter_var(isset($_REQUEST['task']), FILTER_SANITIZE_STRING) ? $_REQUEST['task'] : '';

if ($task == "register") {
    $fname = filter_var(isset($_POST['fname']), FILTER_SANITIZE_STRING) ? $_POST['fname'] : '';
    $lname = filter_var(isset($_POST['lname']), FILTER_SANITIZE_STRING) ? $_POST['lname'] : '';
    $name = $fname . "," . $lname;
    $phone = filter_var(isset($_POST['phone']), FILTER_SANITIZE_STRING) ? $_POST['phone'] : '';
    $email = filter_var(isset($_POST['email']), FILTER_SANITIZE_EMAIL) ? $_POST['email'] : '';
    $address = filter_var(isset($_POST['address']), FILTER_SANITIZE_STRING) ? $_POST['address'] : '';
    $profession = filter_var(isset($_POST['profession']), FILTER_SANITIZE_STRING) ? $_POST['profession'] : '';
    $city = filter_var(isset($_POST['city']), FILTER_SANITIZE_STRING) ? $_POST['city'] : '';
    $state = filter_var(isset($_POST['state']), FILTER_SANITIZE_STRING) ? $_POST['state'] : '';
    $zip = filter_var(isset($_POST['zip']), FILTER_SANITIZE_NUMBER_INT) ? $_POST['zip'] : '';
    $adult = filter_var(isset($_POST['adults']), FILTER_SANITIZE_NUMBER_INT) ? $_POST['adults'] : '';
    $kids = filter_var(isset($_POST['kids']), FILTER_SANITIZE_NUMBER_INT) ? $_POST['kids'] : '';
    $suggestion = filter_var(isset($_POST['suggestion']), FILTER_SANITIZE_STRING) ? $_POST['suggestion'] : '';
    $token = isset($_POST[base64_encode('token')]) ? $_POST[base64_encode('token')] : '';
    $submissiondate = $dated;
    $submission_source = filter_var(isset($_POST['submission_source']), FILTER_SANITIZE_STRING) ? $_POST['submission_source'] : '';
    $modetator = 0;
    $status = 0;
    if (!checkToken($token)) {
        echo "Invalid Token";
        exit;
    }
    $str = "asdfghjklzxcvbnmqwertyuiop0123456987QAZWSXEDCRFVTGBYHNUJMIKOLP";
    $token = substr(str_shuffle($str), 0, 10);

    //  Field validation
    function is_email($email) {
        return preg_match('|^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$|i', $email);
    }

    if (strlen($fname) > 0 && strlen($lname) > 0 && strlen($phone) >= 11 && (is_numeric($zip) && is_numeric($adult) && is_numeric($kids) && is_email($email))) {
        $sql = "INSERT INTO `event` (
        `name`,
        `phone`, 
        `email`, 
        `address`, 
        `profession`, 
        `city`, 
        `state`, 
        `zip`, 
        `adult`, 
        `kids`, 
        `suggestion`, 
        `token`,
        `submissiondate`,
        `submission_source`, 
        `modetator`, 
        `status`)
        VALUES ('$name',$phone,'$email','$address','$profession','$city','$state',$zip,$adult,$kids,'$suggestion','$token','$submissiondate','$submission_source',$modetator,$status)";
        $result = $conn->query($sql);
    } else {
        $_SESSION['message'] = "Something went wrong with registration. Please input all field with correct data";
        echo "registrationError";
        exit;
    }
    $mail = new PHPMailer();
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.revo-interactive.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'mailer@revo-interactive.com';                 // SMTP username
    $mail->Password = '3i5wU2tn';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;
    $mail->setFrom('mailer@revo-interactive.com', 'Event Registration');
    //$mail->addReplyTo('sendinfo98@gmail.com', 'Information');
    $to = $email;
    $mail->AddAddress($to);
    $mail->Subject = "PHPMailer Test Subject via smtp, basic with authentication";
    $mail->Body = "hello, Please click the link verify your mail." . $site_url . "functions.php?token=" . $token . "&email=" . $to . "&task=verify";
    if (!$mail->send()) {
        //echo "Mailer Error: " . $mail->ErrorInfo;
        $_SESSION['message'] = "Something went wrong with registration. Please input all field with correct data";
        echo "registrationError";
        exit;
    } else {
        $_SESSION['message'] = "Thank your, Registration has been success Please check mail to verify and collect ticket.";
        echo "register";
    }
} elseif ($task == "verify") {
    $token = filter_var(isset($_GET['token']), FILTER_SANITIZE_STRING) ? $_GET['token'] : '';
    $email = filter_var(isset($_GET['email']), FILTER_SANITIZE_STRING) ? $_GET['email'] : '';

    if ($token != '' && $email != '') {

        $sql = "SELECT * FROM `event` WHERE `email`='$email' AND `token`='$token' AND `status`=0";

        $result = $conn->query($sql);

        if ($result) {
            $row = $result->num_rows;
            $result = $result->fetch_object();
        } else {
            $_SESSION['message'] = "Something went wrong Please again try to verify";
            header('Location: index.php');
        }

        if ($row == 1) {
            $id = $result->id;
            $email = $result->email;
            $sql = "UPDATE `event` SET `status`=1 WHERE `email`='$email' AND `token`='$token' AND `status`=0";
            $result = $conn->query($sql);
            if ($result) {
                $ticket = $id + 250;
                $ticket = str_pad($ticket, 5, 0, STR_PAD_LEFT);
                $mail = new PHPMailer();
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.revo-interactive.com'; // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'mailer@revo-interactive.com';                 // SMTP username
                $mail->Password = '3i5wU2tn';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;
                $mail->setFrom('mailer@revo-interactive.com', 'Event Registration');
                //$mail->addReplyTo('sendinfo98@gmail.com', 'Information');
                $to = $email;
                $mail->AddAddress($to);
                $mail->Subject = "Ticket";
                $mail->Body = "hello, This is your ticket Number." . $ticket . "Thank you";

                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {
                    $_SESSION['message'] = "Thank you for verifing your mail. Ticket has been send to your mail. If not found check spam. ";
                    header('Location: index.php');
                }
            } else {
                $_SESSION['message'] = "Something went wrong Please again try to verify";
                header('Location: index.php');
            }
        } else {
            $_SESSION['message'] = "Something went wrong Please again try to verify";
            header('Location: index.php');
        }
    }
} else if ($task == "login") {
    $email = filter_var(isset($_POST['email']), FILTER_SANITIZE_STRING) ? $_POST['email'] : '';
    $password = filter_var(isset($_POST['password']), FILTER_SANITIZE_STRING) ? $_POST['password'] : '';

    function is_email($email) {
        return preg_match('|^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$|i', $email);
    }

    if ($password != '' && is_email($email)) {
        $sql = "SELECT * FROM `users` WHERE `email`='$email' AND `password`='$password'";
        $result = $conn->query($sql);
        $result = $result->fetch_object();
        if ($result) {
            $_SESSION["type"] = $result->type;
            $_SESSION["userid"] = $result->id;
            $type = $result->type;
            if ($type == 1) {
                header('Location: admin/dashboard.php');
            } else {
                header('Location: admin/checkticket.php');
            }
        } else {
            $_SESSION["message"] = "Invalid Username Or Password";
            header('Location: admin/index.php');
        }
    } else {
        $_SESSION["message"] = "Invalid Username Or Password";
        header('Location: admin/index.php');
    }
} else if ($task == "ticketCheck") {
    $ticket = filter_var(isset($_POST['ticket']), FILTER_SANITIZE_STRING) ? $_POST['ticket'] : '';
    if ($ticket != '') {

        $ticket = $ticket - 250;
        $sql = "SELECT * FROM `event` WHERE `id`='$ticket' AND `event`.`status` != 0";
        $result = $conn->query($sql);
        $result = $result->fetch_object();
        if ($result) {
            if ($result->status == 2) {
                $_SESSION['message'] = "Ticket Used";
                header('Location: admin/checkticket.php');
            }
            $_SESSION['ticket'] = $result->id;
            $_SESSION['adult'] = $result->adult;
            $_SESSION['kids'] = $result->kids;
            header('Location: admin/welcome.php');
        } else {
            $_SESSION["message"] = "Invalid Ticket";
            header('Location: admin/checkticket.php');
        }
    } else {
        $_SESSION["message"] = "Invalid Ticket";
        header('Location: admin/checkticket.php');
    }
} else if ($task == "attend") {
    $ticket = $_SESSION['ticket'];
    $adult = filter_var(isset($_POST['adults']), FILTER_SANITIZE_STRING) ? $_POST['adults'] : '';
    $kids = filter_var(isset($_POST['kids']), FILTER_SANITIZE_STRING) ? $_POST['kids'] : '';
    $modetator = $_SESSION['userid'];

    if ($ticket != '' && $kids != '' && $adult != '') {
        $sql = "UPDATE `event` SET `status` = 2,`kids` =$kids,`adult`=$adult, `modetator` =$modetator WHERE `event`.`id` = $ticket AND `event`.`status` != 0";
        $result = $conn->query($sql);
        if ($result) {
            echo "updated";
            $_SESSION["message"] = "Thank you for participation";
            header('Location: admin/checkticket.php');
        } else {
            $_SESSION["message"] = "Update fail something went wrong";
            header('Location: admin/checkticket.php');
        }
    } else {
        $_SESSION["message"] = "Update fail something went wrong";
        header('Location: admin/checkticket.php');
    }
} else if ($task == "dashboard") {
    echo "Admin";
    exit;
} else {
    echo "invalid_operation";
}
?>