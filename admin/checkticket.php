<?php
include("../config.php");
include("../common.php");

if (!isset($_SESSION['type'])) {
    header("Location: " . $site_url . "admin/index.php");
}
?>
<?php include('header.php');?>
<div class="page-content container" style="height:880px">
    <?php if (isset($_SESSION['message'])): ?>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading msg">
                        <?php
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-wrapper">
                <div class="box">
                    <form action="../functions.php" method="POST">
                        <div class="content-wrap">
                            <h6>Check Ticket</h6>

                            <input class="form-control" type="text" placeholder="Ticket No" name="ticket">
                            <input type="hidden" name="task" value="ticketCheck" />
                            <div class="action">
                                <button type="submit" class="btn btn-primary signup">Check</button>
                            </div>                
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>