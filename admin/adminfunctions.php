<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function dashboard($conn) {
    $sql = "SELECT count(*) as totalRegistration, sum(`adult`) as adult, sum(`kids`) as kids FROM `event`";
    $result = $conn->query($sql);

    $result = $result->fetch_object();
    $row  =$result->totalRegistration;
    $_SESSION['totalRegistration'] = $result->totalRegistration;
    $_SESSION['adult'] = $result->adult;
    $_SESSION['kids'] = $result->kids;

    $sql = "SELECT count(*) as aTotal, sum(`adult`) as aAdult, sum(`kids`) as aKids FROM `event` WHERE `status` = 2";
    $result = $conn->query($sql);
    $result = $result->fetch_object();
    $_SESSION['aTotal'] = $result->aTotal;
    $_SESSION['aAdult'] = $result->aAdult;
    $_SESSION['aKids'] = $result->aKids;
}

function checkStatus($conn) {
    global $pagination;
    $sql = "SELECT * FROM `event` ORDER BY `id` DESC ";
    $data = $conn->query($sql);
    $rows = $data->num_rows;

    $size = 20;
    $page = isset($_GET['page'])?$_GET['page']:1;
    $pagination->setLink("checkregistration.php?page=%s");
    $pagination->setPage($page);
    $pagination->setSize($size);
    $pagination->setTotalRecords($rows);
    $sql .= $pagination->getLimitSql();
    $data = $conn->query($sql);
    return $data;
}