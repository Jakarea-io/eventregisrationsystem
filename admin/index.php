<?php
include("../config.php");
include("../common.php");
include("adminfunctions.php");
if (isset($_SESSION['type'])) {
    if ($_SESSION['type'] != 1) {
        header("Location: " . $site_url . "admin/checkticket.php");
    } else {
        header("Location: " . $site_url . "admin/dashboard.php");
    }
}
dashboard($conn);
?>
<html>
    <head>
        <title>Event Registration</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- styles -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/styles.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-bg">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <!-- Logo -->
                        <div class="logo">
                            <h1><a href="">Event Authority</a></h1>
                        </div>
                    </div>

                    <div class="col-md-2 pull-right">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li><a href=""></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content container" style="height: 880px">
            <div class="row">

                <?php if (isset($_SESSION['message'])): ?>

                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-danger">
                            <div class="panel-heading msg">
                                <?php
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                                ?>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-wrapper">
                        <div class="box">
                            <form action="../functions.php" method="POST">
                                <div class="content-wrap">
                                    <h6>Sign In</h6>


                                    <input class="form-control" type="text" placeholder="E-mail address" name="email">
                                    <input class="form-control" type="password" placeholder="Password" name="password">
                                    <input type="hidden" name="task" value="login">
                                    <div class="action">
                                        <button type="submit" class="btn btn-primary signup" href="index.html">Login</button>
                                    </div>                
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include('../footer.php'); ?>