<?php
include("../config.php");
include("../common.php");

if (!isset($_SESSION['type']) && !isset($_SESSION['userid'])) {
    header("Location: " . $site_url . "admin/index.php");
}
if (!isset($_SESSION['ticket']) && !isset($_SESSION['adult']) && !isset($_SESSION['kids'])) {
    header("Location: " . $site_url . "checkticket.php");
}
$adult = $_SESSION['adult'];
$kids = $_SESSION['kids'];
$ticket = $_SESSION['ticket'] + 250;
$ticket = str_pad($ticket, 5, 0, STR_PAD_LEFT);
?>
<?php include('header.php');?>
<div class="page-content container" style="height: 880px">
            <?php if (isset($_SESSION['message'])): ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-warning">
                            <div class="panel-heading msg">
                                <?php
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-wrapper">
                        <div class="box">
                            <form action="../functions.php" method="POST">
                                <div class="content-wrap">
                                    <h6>Participent</h6>
                                    <div class="form-group">
                                        <div class="panel panel-success">
                                            <div class="panel-heading msg">Valid Ticket<br> Ticket No : <?php echo $ticket; ?> </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Number of Adults</label>
                                            <select class="form-control" name="adults">
                                                <option  <?php if ($adult == 1) echo"selected"; ?> value="1">1</option>
                                                <option <?php if ($adult == 2) echo"selected"; ?> value="2">2</option>
                                                <option <?php if ($adult == 3) echo"selected"; ?> value="3">3</option>
                                                <option <?php if ($adult == 4) echo"selected"; ?> value="4">4</option>
                                                <option <?php if ($adult == 5) echo"selected"; ?> value="5">5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Number of Kids</label>
                                            <select class="form-control" name="kids">
                                                <option <?php if ($kids == 1) echo"selected"; ?> value="1">1</option>
                                                <option <?php if ($kids == 2) echo"selected"; ?> value="2">2</option>
                                                <option <?php if ($kids == 3) echo"selected"; ?> value="3">3</option>
                                                <option <?php if ($kids == 4) echo"selected"; ?> value="4">4</option>
                                                <option <?php if ($kids == 5) echo"selected"; ?> value="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="task" value="attend" />
                                    <div class="action">
                                        <a href="checkticket.php" class="btn btn-primary signup">Check Another</a>
                                        <button type="submit" class="btn btn-primary signup">Update</button>
                                    </div>     
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include ('footer.php'); ?>