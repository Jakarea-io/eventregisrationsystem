<?php
include("../config.php");
include('class.pagination.php');
include("../common.php");
include("adminfunctions.php");

$pagination = new Pagination();
if (isset($_SESSION['type']) && isset($_SESSION['type'])) {
    if ($_SESSION['type'] != 1) {
        header("Location: " . $site_url . "admin/checkticket.php");
    }
} else {
    header("Location: " . $site_url . "admin/index.php");
}
$result = checkStatus($conn);
$data = array('r' => $result);
?>
<?php include('header.php'); ?>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <?php include('sidebar.php');?>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Show Registration</div>

                            <div class="panel-options">
                                <a href="checkregistration.php" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Ticket No</th>
                                        <th>Adult</th>
                                        <th>Kids </th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Profession</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>zip</th>
                                        <th>suggestion</th>
                                        <th>Registered</th>
                                        <th>Verified</th>
                                        <th>Attended</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    while ($s = $data['r']->fetch_object()) {
                                        $sts = $s->status;
                                        $name = (explode(",", $s->name));
                                        $id = $s->id;
                                        $ticket = str_pad($id, 4, 0, STR_PAD_LEFT) + 250;
                                        $suggestion = $s->suggestion;
                                        ?>
                                        <tr>
                                            <td><?php echo $ticket; ?></td>
                                            <td><?php echo $s->adult; ?></td>
                                            <td>
                                                <?php
                                                if ($s->kids == 0)
                                                    echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
                                                else {
                                                    echo $s->kids;
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $name[0]; ?></td>
                                            <td><?php echo $name[1]; ?></td>
                                            <td><?php echo $s->profession; ?></td>
                                            <td><?php echo $s->city; ?></td>
                                            <td><?php echo $s->state; ?></td>
                                            <td><?php echo $s->zip; ?></td>
                                            <td> 
                                                <a href="#" data-toggle="tooltip" title="<?php echo $s->suggestion; ?>">Show suggestion</a>     
                                            </td>

                                            <td><?php echo "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>"; ?></td>
                                            <td><?php
                                                if (($sts == 1) || ($sts == 2 )) {
                                                    echo "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>";
                                                } else {
                                                    echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if (($sts == 2)) {
                                                    echo "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>";
                                                } else {
                                                    echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
                                                }
                                                ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="pull-right"><?php echo $pagination->create_links(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>