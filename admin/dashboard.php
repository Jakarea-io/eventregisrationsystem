<?php
include("../config.php");
include("../common.php");

include("adminfunctions.php");
if (isset($_SESSION['type'])) {
    if ($_SESSION['type'] != 1) {
        header("Location: " . $site_url . "admin/checkticket.php");
    }
} else {
    header("Location: " . $site_url . "admin/index.php");
}
?>
<?php include('header.php'); ?>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <?php include('sidebar.php'); ?>
        </div>
        <div class="col-md-10">
            <div class="content-box-large">
                <div class="panel-heading">
                    <h4>Registration Summery</h4>
                   
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['totalRegistration']; ?></p> 
                                <p class="summery-body">Number of registration </p> 
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['adult']; ?></p> 
                                <p class="summery-body">Adults registration </p> 
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['kids']; ?></p> 
                                <p class="summery-body">kids registration </p> 
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['aTotal']; ?></p> 
                                <p class="summery-body">Number of attends </p> 
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['aAdult']; ?></p> 
                                <p class="summery-body">Adults attends</p> 
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <div class="panel panel-primary"> 
                                <p class="summery-title"><?php echo $_SESSION['aKids']; ?></p> 
                                <p class="summery-body">Kids attends </p> 
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div>
<?php include('footer.php'); ?>