<?php

function pr($a) {
    echo "<pre>";
    print_r($a);
    echo "</pre>";
}

function token() {
    if (isset($_SESSION) && session_id() != '') {
        $token = new stdClass();
        $token->id = base64_encode("token");
        $token->val = md5(uniqid());
        $_SESSION[$token->id] = $token->val;
        return $token;
    } else {
        return 'session_error';
    }
}

function checkToken($t) {
    if (isset($_SESSION) && session_id() != '' && isset($_SESSION[base64_encode("token")])) {
        if ($t == $_SESSION[base64_encode("token")]) {
            unset($_SESSION[base64_encode("token")]);
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getUserId() {
    if (isset($_SESSION) && session_id() != '') {
        return isset($_SESSION["uid"]) ? $_SESSION["uid"] : 1;
    } else {
        return 1;
    }
}

function getUserData() {
    global $conn;
    if (getUserId() == 0) {
        return false;
    }
    $query = "SELECT * FROM `users` WHERE `id` = " . getUserId() . " LIMIT 1";
    $result = mysqli_query($conn, $query);
    $data = mysqli_fetch_object($result);
    return $data;
}

function getUserQuotes() {
    global $conn;
    if (getUserId() == 0) {
        return false;
    }
    $data = array();
    $query = "SELECT * FROM `images` WHERE `uid` = " . getUserId() . "";
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_object($result)) {
        $data[] = $row;
    }
    return $data;
}

function getQuotes($uid = 0, $limit = 5) {
    global $conn;
    global $pagination;
    $page = isset($_GET['page']) ? $_GET['page'] : 1;

    $data = array();
    $query = "
				SELECT `quotes`.*,`users`.`fb_id`,`users`.`full_name`,`users`.`email`,`users`.`phone`
				FROM `quotes`,`users` WHERE `quotes`.`uid` = `users`.`id` ";
    if ($uid > 0) {
        $query .= " AND `users`.`id` = $uid";
    }
    $query .= " ORDER BY `quotes`.`dated` DESC ";
    $result = mysqli_query($conn, $query);
    $num_rows = mysqli_num_rows($result);

    $paginationLink = "index.php?p=happiness-gallery";
    if ($uid > 0) {
        $paginationLink .= "&t=user";
    }
    $paginationLink .= "&page=%s";


    $pagination->setLink($paginationLink);
    $pagination->setPage($page);
    $pagination->setSize($limit);
    $pagination->setTotalRecords($num_rows);

    $query = $query . $pagination->getLimitSql();


    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_object($result)) {
        $data[] = $row;
    }
    return $data;
}

function getUserPhoneExists() {
    $data = getUserData();
    if ($data->phone != NULL) {
        return true;
    } else {
        return false;
    }
}

?>