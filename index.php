<?php
include("config.php");
include("common.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Registration</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Main CSS -->
        <link rel="stylesheet" href="css/style.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="https://maps.googleapis.com/maps/api/js"></script>


    </head>
    <body>

        <div id="fb-root"></div>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <div id="fb-root"></div>
        <div id="BodyLoader" style="display:block; width:100%; height:100%; z-index:1200; background:rgba(255,255,255,.9); position:absolute; left:0; top:0;">
            <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><img src="img/ajax-loader.gif" height="64px;" /></div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"><img src="img/logo.png" class="img-responsive" /></div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8"><img src="img/header-banner.jpg" class="img-responsive img-center" /></div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <!--<nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>
                        Ugadi 2016 Mega Event by ManaTV. Entry is FREE if you Pre-Register or Pay $10.00 Per Person at Venue without Registration!
                    </h1>
                    <p>Please fill out your information below to register for this event.</p>
                </div>
            </div>
          </div>
        </nav>-->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img src="img/kids-platform-banner.jpg" class="img-responsive img-center" />
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <?php if (isset($_SESSION['message'])): ?>

                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-danger">
                            <div class="panel-heading msg">
                                <?php
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <form role="form" class="form-horizontal" id="regi_form">
                        <div class="form-group">
                            <div class="col-sm-6"><label for="fname">First name</label><input id ="fname" type="text" name="fname" class="form-control" placeholder="First Name" required></div>
                            <div class="col-sm-6"><label for="lname">Last name</label><input id="lname" type="text" name="lname" class="form-control" placeholder="Last Name" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label for="phone">Phone number</label><input id="phone" type="text" name="phone" class="form-control" placeholder="Phone Number" minlength="11" maxlength="17" required ></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label for="email">Email</label><input id ="email" type="email" name="email" class="form-control" placeholder="Please enter a valid email to receive tickets" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label for="profession">Profession</label><input id="profession" type="text" name="profession" class="form-control" placeholder="Profession" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label for="address" >Address</label><input id="address" type="text" name="address" class="form-control" placeholder="Address" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"><label for="zip">Zip Code</label><input id="zip" type="number" name="zip" class="form-control" placeholder="Zip Code" required></div>
                            <div class="col-sm-4"><label for="city">City</label><input id="city" type="text" name="city" class="form-control" placeholder="City" required></div>
                            <div class="col-sm-4"><label for="state">State</label><input id="state" type="text" name="state" class="form-control" placeholder="State" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="suggestion">Please Specify Type of Shows you would like to Watch on ManaTV?</label>
                                <input type="text" name="suggestion" class="form-control" placeholder="Your suggestion">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6"><label>Number of Adults</label>
                                <select class="form-control" name="adults" required>
                                    <option value="">Number of Adults</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="col-sm-6"><label>Number of Kids</label>
                                <select class="form-control" name="kids" required>
                                    <option value="">Number of Kids</option>
                                    <option value="0">None</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group mar-bot-30">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" name="task" value="register" />
                                <input type="hidden" name="<?php echo token()->id; ?>" value="<?php echo token()->val; ?>" />
                                <?php $submission_source = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>
                                <input type="hidden" name="submission_source" value="<?php echo $submission_source; ?>" />
                                <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg pull-right">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs mar-top-30">
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Digitization and Software Development by <a href="http://hireitpeople.com/">HireITPeople</a></p>
                    </div>
                </div>
            </div>
        </footer>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Social share</h4>
                    </div>
                    <div class="modal-body">
                        <p class="modal-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>


                        <div class="row onl_socialButtons">
                            
                            <div class="col-xs-4 col-sm-4 ">
                                <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"></div>
                            </div>
                            
                            <div class="col-xs-4 col-sm-4">
                                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                            </div> 
                            
                            <div class="col-xs-4 col-sm-4">
                                <script src="https://apis.google.com/js/platform.js" async defer></script>
                                <div class="g-plus" data-action="share"  data-annotation="bubble" data-width="100%"></div>
                            </div>
                            
                        </div><!-- /.modal-content -->

                    </div><!-- /.modal -->

                    <script src="http://connect.facebook.net/en_US/all.js"></script>
                    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                    <!-- Latest compiled and minified JavaScript -->
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                    <script src="js/main.js"></script>

                    <script type="text/javascript">

                    </script>
                    </body>
                    </html>