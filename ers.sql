-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 09:49 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ers`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `phone` varchar(17) NOT NULL,
  `email` varchar(80) NOT NULL,
  `profession` varchar(60) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zip` int(11) NOT NULL,
  `suggestion` varchar(160) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adult` int(11) NOT NULL,
  `kids` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `modetator` int(1) NOT NULL,
  `submission_source` varchar(20) NOT NULL,
  `token` varchar(32) NOT NULL,
  `submissiondate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `name`, `phone`, `email`, `profession`, `address`, `city`, `state`, `zip`, `suggestion`, `adult`, `kids`, `status`, `modetator`, `submission_source`, `token`, `submissiondate`) VALUES
(1, 'Jakara,Parvez', '125478965', 'jakara@dfdf.codk', 'dfdf', 'Kahjdhfe', 'Dhaka', 'Kahaloo', 120, '2', 4, 0, 2, 2, '', '123', '2016-03-28 20:08:15'),
(2, 'Jakarea,Zehad', '2147483647', 'send@9o.com', 'Student', 'Mouchak', 'Dhaka', '0', 1201, '2', 5, 5, 1, 1, '', '1234', '2016-03-28 20:13:20'),
(3, 'Parvez,zEHAD', '123145878', 'sendinfo98@gmail.com', 'Student', 'Kahaloo', 'Dhaka', '0', 1202, '1', 1, 1, 2, 1, '', '', '2016-03-28 20:34:24'),
(4, 'dfdf,dfdf', '103021457', 'test@mail.com', '', 'dfd dfdfd ', 'dfdfd dfd', '0', 120, '2', 1, 1, 2, 1, 'localhost/ers/', 'ToVIJih48t', '2016-03-29 13:31:51'),
(5, 'dsfds, fdf6d5f6d', '5612656', '123@123.com', 'dfdf', 'dfdf', 'dfd', '0', 20, '3', 3, 1, 2, 1, 'localhost/ers/', 'gzyepB14ZE', '2016-03-29 13:34:49'),
(6, '5f14d256, dfdf6', '645246', '321@321.com', 'd12fd', '', 'df5', '0', 25, '4', 4, 4, 2, 1, 'localhost/ers/', 'ZKPgxhH5Xt', '2016-03-29 13:39:23'),
(7, 'Abar test,Tst Abar', '123', '145@555.com', 'Student', 'Bogra', 'Dhaka', '0', 120, '', 2, 3, 2, 1, 'localhost/ers/', 'ZPa1TmyAVf', '2016-03-29 14:29:32'),
(8, 'Registration,Verify', '14578', 'verify@mail.com', '123', 'dfd', 'dfdfd', '0', 102, '2', 5, 5, 1, 0, 'localhost/ers/', 'X8Jc6lIeKE', '2016-03-29 14:35:36'),
(9, 'asdfsdsfds,kljhi ygyug', '123577215478', '1222484@fdfd', 'dfdfd', 'fdfdfd dfd', 'fdfdf', '0', 1235, '1', 1, 0, 0, 0, 'localhost/ers/', 'OsFHcQ0aod', '2016-03-30 11:56:22'),
(10, 'Jakarea,Parvez', '1323021458745', '2ndtest@mail.com', '2ndtest', '2ndtest', '2ndtest', '2ndtest', 1201, '1', 5, 5, 0, 0, 'localhost/ers/index.', 'CTKiMme1aP', '2016-03-30 12:06:41'),
(11, 'Aftab,Zaman', '1236547891', 'aftab@em.com', 'Studenr', 'rAMPURA', 'dHAKA', 'Dhaka', 1023, '1', 4, 4, 1, 0, 'localhost/ers/index.', 'Z7zHGqF6tB', '2016-03-30 12:28:52'),
(12, 'ajim,khan', '1236547895', 'ajim@khan', 'Actor', 'Dillih', 'Dhaka', 'Sylhet', 1412, '1', 3, 3, 0, 0, 'localhost/ers/index.', 'u94gs6GVBP', '2016-03-30 12:42:20'),
(13, 'ajim,khan', '1236547895', 'ajim@khan', 'Actor', 'Dillih', 'Dhaka', 'Sylhet', 1412, '1', 3, 3, 0, 0, 'localhost/ers/index.', 'PvxRfAtuIW', '2016-03-30 12:42:53'),
(14, 'ajim,khan', '1236547895', 'ajim@khan', 'Actor', 'Dillih', 'Dhaka', 'Sylhet', 1412, ';z jkzj zkj klj f', 3, 3, 0, 0, 'localhost/ers/index.', 'fQhd4l0rJt', '2016-03-30 12:44:04'),
(15, 'Sakib,Khan', '123654789524', 'sakib@em.com', 'Teacher', 'Noakhali', 'Dhaka', 'Rangpur', 5421, 'lkj   kl klj kl l dzfs klj klj', 2, 0, 1, 0, 'localhost/ers/index.', 'Rc3Yy8KrqI', '2016-03-30 12:45:57'),
(16, 'Event,Registration', '12365478910', 'event@regi.com', 'Evnt Manager', 'Cumillah', 'Borishal', 'Dhaka', 5214, 'juidasg kashjkladfkljs kldfjsl', 1, 2, 0, 0, 'localhost/ers/', 'v3PlQ7fouz', '2016-03-30 12:52:54'),
(18, 'Jakarea,Parevez', '1728247398', 'jakareaparvez@gmail.com', 'Student', 'Kahaloo', 'Bogra', 'Dhaka', 1219, 'aas as a a das', 5, 5, 0, 0, 'localhost/ers/', 'gKbQiErkTD', '2016-03-30 19:49:46'),
(170, 'Anoar,Hoswn', '10236547896547', 'anor@e.co', 'Bekar', 'Bekaj', 'Japan', 'Panama', 1021, 'ERS is a registration system for events. User register', 2, 3, 0, 0, 'localhost/ers/index.', 'CWiDV0OhIM', '2016-03-30 13:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `phone`, `type`) VALUES
(1, 'admin', 'welcome', 'admin@mail.com', 1, 1),
(2, 'volunteer', 'welcome', 'volunteer@mail.com', 789654123, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
