$(document).ready(function () {


    // ###############################
    // Preload Images
    // ###############################
    var preloadPictures = function (pictureUrls, callback) {
        var i, j, loaded = 0;

        for (i = 0, j = pictureUrls.length; i < j; i++) {
            (function (img, src) {
                img.onload = function () {
                    if (++loaded == pictureUrls.length && callback) {
                        callback();
                    }
                };
                img.onerror = function () {};
                img.onabort = function () {};
                img.src = src;
            }(new Image(), pictureUrls[i]));
        }
    };
    var i = 0;
    var img = new Array();
    $('#imageGallery img').each(function (index, element) {
        $("<style type='text/css'> .imageLoader{ display:block; background:url(img/ajax-loader.gif) no-repeat center center; background-size:10% 10%; height:180px; width:270px; } </style>").appendTo("head");
        $(this).addClass('imageLoader');
        i++;
        img[i] = new Image();
        img[i].onload = function () {
            $(this).removeClass('imageLoader');
        };
        img[i].src = $(this).attr('src');
    });

    var preLoadImages = ['img/logo.png'];
    preloadPictures(preLoadImages, function () {
        $('.container-fluid').show();
        $('#BodyLoader').fadeOut(300);
    });


    // ###############################
    // Submit Happiness
    // ###############################

    $("#regi_form").submit(function (e) {
        e.preventDefault();
        submitRegistration();
    });

    function createImage(p) {
        var quote = $('#happinessis').val();
        if (quote == '') {
            alert("Please enter your happiness quote");
            $('#BodyLoader').fadeOut(300);
            return false;
        }
        canvas = html2canvas($("#imageCanvas"), {
            onrendered: function (canvas) {
                img = Canvas2Image.convertToPNG(canvas, 1200, 471);
                $("#imageCanvas").html(img);
                submitQuote(p);
            }
        });
    }

    function submitRegistration() {
        $('#BodyLoader').fadeIn(300);
        var data = $('#regi_form').serialize();
        console.log(data);
        $.ajax({
            url: 'functions.php',
            data: data,
            type: 'POST',
            success: function (res) {
                console.log("Response:  " + res);
                if (res == "auth") {
                    window.location = 'checkTicket.php?msg=1';
                } else if (res == "register") {
                    window.location = 'index.php';
                    $('#BodyLoader').fadeOut(300);
                } else if (res == "registrationError") {
                    window.location = 'index.php';
                    $('#BodyLoader').fadeOut(300);
                } else if (res == "validTicket") {
                    window.location = 'welcome.php';
                } else if (res == "updated") {
                    window.location = 'checkTicket.php';
                } else if (res == 'error' || res == 'invalid') {
                    alert('Unknown error!');
                    return;
                } else {
                    FB.ui({
                        method: 'feed',
                        link: 'http://revo-apps.com/demo/event-registration/'
                    }, function (response) {

                    });
                }
                $('#BodyLoader').fadeOut(300);
            },
            error: function (e) {
                window.location = 'index.php';
                $('#BodyLoader').fadeOut(300);
                console.log(e);
            }
        });
    }

    $('#infoSaveBtn').click(function (e) {
        var email = $('#info-email');
        var phone = $('#info-phone');
        var address = $('#info-address');
        var error = 0;
        if (!isValidEmailAddress(email.val())) {
            email.parent().addClass('has-error');
            error++;
        }
        if (!isValidPhone(phone.val())) {
            phone.parent().addClass('has-error');
            console.log(phone.parent().attr('class'))
            error++;
        }
        if (error > 0) {
            alert("Please provide valid information");
            return false;
        }

        $('#BodyLoader').fadeIn(300);

        var data = new FormData();
        data.append('task', 'updateinfo');
        data.append('email', email.val());
        data.append('phone', phone.val());
        data.append('address', address.val());
        data.append('uid', $("#suid").val());

        jQuery.ajax({
            url: 'functions.php',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if (data == 'success') {
                    $('#BodyLoader').fadeOut(300);
                    window.location = "index.php?p=happiness-gallery&t=user";
                } else {
                    alert("Error updating information. Please try again later.");
                    $('#BodyLoader').fadeOut(300);
                }
            },
            error: function () {
                alert("Error updating information. Please try again later.");
            }
        });
    });

    // ###############################
    // Modal
    // ###############################

    $('[data-load-remote]').on('click', function (e) {
        e.preventDefault();
        var remote = $(this).data('load-remote');
        var size = $(this).data('size');
        $('#MainDialog').hide();
        $('#ModalLoader').show();
        i = 0;
        $.ajax({
            url: remote,
            dataType: 'html',
            error: function () {
                console.log('An error has occurred loading the page.Please try again later.');
            },
            success: function (data) {
                $('#MainModalBody').html(data);
                var title = $($.parseHTML(data)).filter("title").text();
                $('#MainModalTitle').html(title);

                if (size == 'sm') {
                    if ($(window).width() > 991) {
                        $('#MainDialog').css("overflow", "scroll");
                        $('#MainDialog').css("height", "400px");
                    }
                } else {
                    $('#MainDialog').css("overflow", "hidden");
                    $('#MainDialog').css("height", "auto");
                }

                var images = new Array();
                $('#MainModalBody').find("img").each(function (index, element) {
                    images[i] = $(this).attr('src');
                    i++;
                });
                if (images.length > 0) {
                    preloadPictures(images, function () {
                        reposition();
                        $('#MainDialog').fadeIn(300).show();
                        $('#ModalLoader').hide();
                    });
                } else {
                    $('#MainDialog').fadeIn(300).show();
                    $('#ModalLoader').hide();
                }
                reposition();
            },
            type: 'GET'
        });
    });

    // Position modal on center
    $('#MainModal').on('hidden.bs.modal', function () {
        $('#MainDialog').hide();
        $('#ModalLoader').show();
        $('#MainModalBody').html('');
    });
    $('#MainModal').on('show.bs.modal', reposition);

    function reposition() {
        var modal = $("#MainModal"),
                dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() / 2) - (dialog.height() / 2)));
    }
    $('.modal').on('show.bs.modal', reposition);
    $(window).on('resize', function () {
        $('.modal:visible').each(reposition);
    });

    // Facebook Init
    FB.init({
        appId: '1154621241224380',
        cookie: true,
        status: true,
        xfbml: true,
        oauth: true
    });
    // Facebook Invite
    $('.invite_friends').click(function (e) {
        e.preventDefault();
        FB.ui({
            method: 'apprequests',
            message: 'You have been invited to use the app Chobite Sundarban'
        });
    });
    // Facebook Login
    $('.fblogin').click(function (e) {
        e.preventDefault();
        FB.login(function (response) {
            if (response.status === 'connected') {
                loggedInViaFacebook(FB);
            }
        },
                {scope: 'public_profile,email'}
        );
    });
    // Facebook Logout
    $('.fblogout').click(function (e) {
        e.preventDefault();
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                FB.logout(function (response) { });
            }
            var data = new FormData();
            data.append('task', 'logout');
            jQuery.ajax({
                url: 'functions.php',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    window.location = 'index.php';
                },
                error: function () {
                    window.location = 'index.php';
                    //alert("Error logout. Please try after refreshing again!");
                }
            });
        });
    });
    //Facebook event check login/logout status
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            console.log('Loggedin via Facebook');
            loggedInViaFacebook(FB);
        } else if (response.status === 'not_authorized') {
            console.log('Please log ' + 'into this app.');
        } else {
            console.log('Please log ' + 'into Facebook.');
        }
    });
    //Facebook check login/logout status function
    function loggedInViaFacebook(FB) {
        FB.api('/me?fields=id,name,gender', function (response) {
            var data = new FormData();
            data.append('task', 'login');
            data.append('fbid', response.id);
            data.append('name', response.name);
            data.append('gender', response.gender);

            jQuery.ajax({
                url: 'functions.php',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    if ($('#suid').val() == 0) {
                        window.location = 'index.php?p=my-happiness';
                    }
                },
                error: function () {
                    console.log("Error Saving User Data");
                }
            });
        });
    }

    function isValidPhone(phone) {
        if (phone.length == 11 && isNaN(phone) == false) {
            return true;
        } else {
            return false;
        }
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    ;


//Zip code and City and state Populate form   Zip code
    var geocoder = new google.maps.Geocoder();

    $('#zip').bind('blur focusout', function () {

        var $this = $(this);

        if ($this.val().length != 5) {
            $('#city').val("Invalid Zip");
            $('#state').val("Invalid Zip");
        } else {

            geocoder.geocode({'address': $this.val()}, function (result, status) {
                if (status == "OK") {
                    for (var component in result[0]['address_components']) {
                        for (var i in result[0]['address_components'][component]['types']) {
                            if (result[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
                                statecode = result[0]['address_components'][component]['short_name'];
                                state = result[0]['address_components'][1]['long_name'];
                                $('#state').val(statecode);
                            }
                            if (result[0]['address_components'][component]['types'][i] == "locality") {
                                city = result[0]['address_components'][component]['long_name'];
                                $('#city').val(city);
                            }
                        }
                    }
                }
            });
        }
    });

    //Suggesion  
    $('[data-toggle="tooltip"]').tooltip();

    $('.example').popover('dismiss');

    //Modal
    $(window).load(function () {
        setTimeout(function () {
            $('#myModal').modal('show');
        }, 3000);
    });

    //Twitter share
    !function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }
    (document, 'script', 'twitter-wjs');
});
