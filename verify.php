<?php
include("config.php");
include("common.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Registration</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Main CSS -->
        <link rel="stylesheet" href="css/style.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="fb-root"></div>
        <div id="BodyLoader" style="display:block; width:100%; height:100%; z-index:1200; background:rgba(255,255,255,.9); position:absolute; left:0; top:0;">
            <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><img src="img/ajax-loader.gif" height="64px;" /></div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"><img src="img/logo.png" class="img-responsive" /></div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8"><img src="img/header-banner.jpg" class="img-responsive img-center" /></div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <!--<nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>
                        Ugadi 2016 Mega Event by ManaTV. Entry is FREE if you Pre-Register or Pay $10.00 Per Person at Venue without Registration!
                    </h1>
                    <p>Please fill out your information below to register for this event.</p>
                </div>
            </div>
          </div>
        </nav>-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="img/kids-platform-banner.jpg" class="img-responsive img-center" />
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <form role="form" class="form-horizontal" id="regi_form">
                        <div class="form-group">
                            <div class="col-sm-6"><label>First name</label><input type="text" name="fname" class="form-control" placeholder="First Name"></div>
                            <div class="col-sm-6"><label>Last name</label><input type="text" name="lname" class="form-control" placeholder="Last Name"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label>Phone number</label><input type="text" name="phone" class="form-control" placeholder="Phone Number"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label>Email</label><input type="email" name="email" class="form-control" placeholder="Email"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label>Profession</label><input type="text" name="profession" class="form-control" placeholder="Profession"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label>Address</label><input type="text" name="address" class="form-control" placeholder="Address"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"><label>City</label><input type="text" name="city" class="form-control" placeholder="City"></div>
                            <div class="col-sm-4"><label>State</label><input type="text" name="state" class="form-control" placeholder="State"></div>
                            <div class="col-sm-4"><label>Zip Code</label><input type="text" name="zip" class="form-control" placeholder="Zip Code"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">How did you hear about this event?</label>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio1" value="1" type="radio"> 
                                    <label for="radio1">Friend or colleague</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio2" value="2" type="radio">
                                    <label for="radio2">ManaTV Promo</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio3" value="3" type="radio"> 
                                    <label for="radio3">WhatsAPP</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio4" value="4" type="radio">
                                    <label for="radio4">Facebook</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio5" value="5" type="radio">
                                    <label for="radio5">Flyer</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="radio-inline">
                                    <input name="source" id="radio6" value="6" type="radio"> 
                                    <label for="radio6">Other:</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><input type="text" name="others" class="form-control" placeholder="Others"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6"><label>Number of Adults</label>
                                <select class="form-control" name="adults">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="col-sm-6"><label>Number of Kids</label>
                                <select class="form-control" name="kids">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group mar-bot-30">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" name="task" value="register" />
                                <input type="hidden" name="<?php echo token()->id; ?>" value="<?php echo token()->val; ?>" />
                                <?php $submission_source = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>
                                <input type="hidden" name="submission_source" value="<?php echo $submission_source; ?>" />
                                <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg pull-right">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs mar-top-30">
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row mar-15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="img/banner.jpg" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Digitization and Software Development by <a href="http://hireitpeople.com/">HireITPeople</a></p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="http://connect.facebook.net/en_US/all.js"></script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="js/main.js"></script>
    </body>
</html>